#!/usr/bin/perl
# Copyright (c) 2021 smpl <smpl@slamkode.ml>
# SPDX-License-Identifier: Zlib
# https://www.rejsekort.dk/da/Salgssteder

#binmode STDOUT, ':encoding(UTF-8)';
use JSON::XS;
use Encode;
use HTML::TreeBuilder 5 -weak;
use LWP;
use LWP::UserAgent;
use strict;

my $ua = LWP::UserAgent->new;
$ua->agent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:76.0) Gecko/20100101 Firefox/76.0");
my $response = $ua->get('https://www.rejsekort.dk/da/Salgssteder');

my $tree = HTML::TreeBuilder->new(ignore_unknown => 0);
$tree->parse_content($response->content);

my $body = $tree->find_by_tag_name('body');
my $data_element = $body->look_down("data-behavior", "map-list");
my $semi_json = encode("UTF-8", $data_element->attr('data-settings'));

my $json = JSON::XS->new->utf8->decode($semi_json);

open(my $fh, ">", "salgssteder.json");
print $fh JSON::XS->new->ascii->encode($json);
close($fh);
