#!/bin/bash
# Copyright (c) 2021 smpl <smpl@slamkode.ml>
# SPDX-License-Identifier: Zlib

URL="https://www.tv2east.dk/live"
USER_AGENT="Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko"
PLAYLIST=""

while read -r line; do
	if [[ "$line" == *"playlist_url:"* ]]; then
		PLAYLIST=$(echo -n $line | sed -n 's/.*playlist_url:"\([^"]*\)".*/\1/p' | sed -n 's/\\u002F/\//gp')
		while read fline; do
			:;
		done
		break;
	fi
done < <(curl -s -A "$USER_AGENT" "$URL")

echo "Playlist: $PLAYLIST"

RESOLUTION=""
URL=""

while read -r line; do
	if [[ $line == "#EXT-X-STREAM-INF"* ]]; then
		RESOLUTION=$(echo -n "$line" | sed -n 's/.*RESOLUTION=\([^,\n]*\)/\1/p')
	elif [[ $line != "#"* ]]; then
		URL=$line;
	fi

	if [[ -n $RESOLUTION && -n $URL ]]; then
		echo "$RESOLUTION:"
		echo " $URL"
		RESOLUTION=""
		URL=""
	fi
done < <(curl -s -A "$USER_AGENT" "$PLAYLIST")
